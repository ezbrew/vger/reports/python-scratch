# https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html
import logging
import boto3
from botocore.exceptions import ClientError

AWS_REGION = 'us-east-1'
# logger config
logger = logging.getLogger()
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s: %(levelname)s: %(message)s')
vpc_client = boto3.client("ec2", region_name=AWS_REGION)

def describe_vpcs():
    """
    Describe the VPC's.
    """
    try:
        response = vpc_client.describe_vpcs()
    except ClientError:
        logger.exception('Could not describe the VPCs')
        raise
    else:
        return response

def return_subnets(vpc_id):
    """
    Describe the subnets.
    """
    try:
        response = vpc_client.describe_subnets(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': [vpc_id],
                },
            ],
        )
    except ClientError:
        logger.exception('Could not describe the VPCs')
        raise

    return_list = []
    for subnet in response['Subnets']:
        subnet_id = subnet['SubnetId']
        subnet_cidr = subnet['CidrBlock']
        print ("subnet_id: {}".format(subnet_id))
        print ("subnet_cidr: {}".format(subnet_cidr))
        subnet_string = '{0}: {1}\n'.format(subnet_id, subnet_cidr)
        return_list.append(subnet_string)
    print ("return_list: {}".format(return_list))
    return return_list

def return_tag_value(tag_name, vpc_tags):
    for tag in vpc_tags:
        if tag['Key'] != tag_name: continue
        print ("tag: {}".format(tag))
        return tag['Value']
    return ''

def get_it_done():
   vpcs_response = describe_vpcs()
#    print (vpcs_response)
   for vpc in vpcs_response['Vpcs']:
       vpc_name = ''
       vpc_id = vpc['VpcId']
       vpc_cidr = vpc['CidrBlock']
       if 'Tags' in vpc:
           vpc_name = return_tag_value('Name', vpc['Tags'])
       else:
           vpc_name = ''
       subnet_list = return_subnets(vpc_id)
       subnet_list_string = "".join(return_subnets(vpc_id))
       print ("\nvpc_name: {}".format(vpc_name))
       print ("vpc_id: {}".format(vpc_id))
       print ("vpc_cidr: {}".format(vpc_cidr))
    #    print ("subnet_list: {}".format(subnet_list))
       print ("subnet_list_string:\n{}".format(subnet_list_string))

get_it_done()
